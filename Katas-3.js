function oneTwenty() {
    const numbers = []
    for (let counter = 1; counter <=20; counter++){
        numbers.push(counter)
    }
    return numbers;
}

let oneThroughTwentyText = document.createTextNode('answer: ' + oneTwenty())

oneThruTwenty.appendChild(oneThroughTwentyText);


function evensTwenty() {
    const numbers = []
    for (let counter = 1; counter <=20; counter++){
        if((counter % 2) === 0)
        numbers.push(counter)

    }
    return numbers;
}

let evensThroughTwentyText = document.createTextNode('answer: ' + evensTwenty())

evensThruTwenty.appendChild(evensThroughTwentyText);



function oddsTwenty() {
    const numbers = []
    for (let counter = 1; counter <=20; counter++){
        if((counter % 2) === 1)
        numbers.push(counter)

    }
    return numbers;
}

let oddsThroughTwentyText = document.createTextNode('answer: ' + oddsTwenty())

oddsThruTwenty.appendChild(oddsThroughTwentyText);


function multiplesOf5() {
    const numbers = []
    for (let counter = 1; counter <=100; counter++){
        if((counter % 5) === 0)
        numbers.push(counter)

    }
    return numbers;
}

let multiplesOfFiveText = document.createTextNode('answer: ' + multiplesOf5())

multiplesOfFive.appendChild(multiplesOfFiveText);



function squaredF() {
    const numbers = []
    for (let counter = 1; counter <=10; counter++){
        i = counter * counter
        numbers.push(i)

    }
    return numbers;
}


let squaredText = document.createTextNode('answer: ' + squaredF())

squared.appendChild(squaredText);



function backwardsTwenty() {
    const numbers = []
    for (let counter = 20; counter >=1; counter--){
        numbers.push(counter)

    }
    return numbers;
}


let backwardsText = document.createTextNode('answer: ' + backwardsTwenty())

backwards.appendChild(backwardsText);


function backwardsEvens2() {
    const numbers = []
    for (let counter = 20; counter >=1; counter--){
        if((counter % 2) === 0)
        numbers.push(counter)
}
return numbers;
}
    

let backwardsEvensText = document.createTextNode('answer: ' + backwardsEvens2())

backwardsEvens.appendChild(backwardsEvensText);


function backwardodds() {
    const numbers = []
    for (let counter = 20; counter >=1; counter--){
        if((counter % 2) === 1)
        numbers.push(counter)
}
return numbers;
}
    

let backwardsOddsText = document.createTextNode('answer: ' + backwardodds())

backwardsOdd.appendChild(backwardsOddsText);


function downMultiples5() {
    const numbers = []
    for (let counter = 100; counter >=1; counter--){
        if((counter % 5) === 0)
        numbers.push(counter)
}
return numbers;
}
    

let downMultiplesText = document.createTextNode('answer: ' + downMultiples5())

downMultiples.appendChild(downMultiplesText);


function downSquared2() {
    const numbers = []
    for (let counter = 10; counter >=1; counter--){
        i = counter * counter
        numbers.push(i)
}
return numbers;
}
    

let downSquaredText = document.createTextNode('answer: ' + downSquared2())

downSquared.appendChild(downSquaredText);


sampleArray1 = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

let sampleArrayText = document.createTextNode('answer: ' + sampleArray1)
sampleArray.appendChild(sampleArrayText);

function evensArray() {
    const numbers = []
    for (let item of sampleArray1){
        if ((item % 2) === 0)
        numbers.push(item)
}
return numbers;
}

let sampleArrayEven = document.createTextNode('answer: ' + evensArray())
sampleArrayEvens.appendChild(sampleArrayEven)

function oddsArray() {
    const numbers = []
    for (let item of sampleArray1){
        if ((item % 2) === 1)
        numbers.push(item)
}
return numbers;
}

let sampleArrayOdd = document.createTextNode('answer: ' + oddsArray())
sampleArrayOdds.appendChild(sampleArrayOdd)

function squaredArray() {
    const numbers = []
    for (let item of sampleArray1){
        i = item * item
        numbers.push(i)
}
return numbers;
}

let sampleArraySquare = document.createTextNode('answer: ' + squaredArray())
sampleArraySquared.appendChild(sampleArraySquare)

function sumOfTwenty(){
    let numbers = 0
    for (let i = 1; i < 20; i++){
       numbers+=i
    }
    return numbers
}

let sum20 = document.createTextNode('answer: ' + sumOfTwenty())
sumOf20.appendChild(sum20)

function sumOfArray(){
    let numbers = 0
    for (let item of sampleArray1){
        numbers+=item
    }
    return numbers
}

let sumArray = document.createTextNode('answer: ' + sumOfArray())
sumArraySample.appendChild(sumArray)

function smallest(){
  let small = Math.min(...sampleArray1) 
  return small 
}

let smallNumber = document.createTextNode('answer: ' + smallest())
smallestNumber.appendChild(smallNumber)

function largest(){
    let large = Math.max(...sampleArray1)
    return large
}

let largeNumber = document.createTextNode('answer: ' + largest())
largestNumber.appendChild(largeNumber)